---
slug: "i-btv1-000"
date: "2022-04-19"
title: "Help a Gimbal Token Group implement this Bounty Tracker"
tags: ["Open"]
scope: "this-project"
ada: 10
gimbals: 500
---

## Outcome: Another Gimbal Token Group has a working implementation of this Treasury, Bounty Escrow, and Front End

## How to commit to this bounty:

If you have an access token, you can commit to contributing on this Bounty.

## How to complete this bounty:

Share a link to a working Group instance of this project.
