import React, { useEffect, useState } from "react";
import { StaticQuery, graphql, Link } from "gatsby";
import { Heading, Text, Box, Flex, Button } from "@chakra-ui/react";

import { useStoreState } from "easy-peasy";
import useWallet from "../../hooks/useWallet";
import { useTxAddresses } from "../../hooks/useTxAddresses";

import { fromHex, toHex, toStr } from "../../utils/converter";
import { createBountyDatum } from "../../utils/factory";
import { distributeBounty } from "../../cardano/bounty-contract";
import { treasuryContractAddress } from "../../cardano/treasury-contract";


const CommittedBounty = (props) => {

    const connected = useStoreState((state) => state.connection.connected);
    const { wallet } = useWallet(null)
    const [walletUtxos, setWalletUtxos] = useState([])
    const [walletAddress, setWalletAddress] = useState(null)
    const [contributorAddress, setContributorAddress] = useState(null)

    // The way array indexes are handled here is not build to scale.
    const utxo = props.utxo
    const slug = utxo.transaction.metadata[0].value
    const lovelace = utxo.value
    const gimbals = utxo.tokens[0].quantity
    const accessToken = utxo.tokens[1].asset.assetName
    const txHash = utxo.transaction.hash
    const txIx = utxo.index

    // This hook gets the input Addresses from the Transaction whose output was the Bounty UTXO
    // One input address is the Treasury, so we can disregard that.
    // The other input address is the Contributor
    const {
        txAddresses,
        getTxAddresses,
        loading: addressesLoading,
        error,
    } = useTxAddresses();

    // To get it working, just hard code the Contributor Address:


    useEffect(async () => {
        if (connected && wallet) {
            await getTxAddresses({
                variables: {
                    txhash: txHash
                },
            });
            const myUtxos = await wallet.utxos;
            setWalletUtxos(myUtxos);
            setWalletAddress(wallet.address)
        }
    }, [wallet])

    useEffect(async () => {
        const transactionAddresses = txAddresses.transactions[0].inputs.map(input => input.address)
        const contributorAddresses = transactionAddresses.filter(address => address != treasuryContractAddress)
        setContributorAddress(contributorAddresses[0])
    }, [txAddresses])

    console.log("Access Token", accessToken)
    console.log("here are some addresses", contributorAddress)



    const handleDistribute = async () => {
        try {
            console.log("DISTRIBUTE A BOUNTY", lovelace, gimbals)
            const bDatum = createBountyDatum(walletAddress, contributorAddress, lovelace, gimbals, 10000000000)
            const bUtxo = {
                "tx_hash": txHash,
                "output_index": txIx,
                "amount": [
                    { "unit": "lovelace", "quantity": `${lovelace}` },
                    { "unit": "cb4a5cb63378a521cb82bdfacc4a8fd543b22ae19c094b75e13f78537447696d62616c", "quantity": `${gimbals}` },
                    { "unit": `5d6b6c332866044b2a8bdd147d92f77e42714986f1cb98cef70e201f${accessToken}`, "quantity": "1" }
                ],
            }

            console.log("bounty UTXO:", bUtxo)

            const bountyDistribution = {
                issuerAddress: walletAddress,
                contributorAddress,
                utxosParam: walletUtxos,
                slug,
                accessTokenName: toStr(fromHex(accessToken)),
                lovelace,
                gimbals,
            }

            distributeBounty(bUtxo, bDatum, bountyDistribution)

        } catch (error) {

        }

    }

    return (
        <Flex w='100%' p='3' m='3' bg="red.200" key={slug}>
            <Box w='60%'>
                <Link to={`/bounties/${slug}`}>
                    <Heading size='md'>
                        Committed Bounty: {slug}
                    </Heading>
                </Link>
                <Text fontSize='xs'>
                    {txHash}#{txIx}
                </Text>
                <Button m='3' onClick={handleDistribute}>Distribute this Bounty</Button>
                <Text>
                    {JSON.stringify(txAddresses)}
                </Text>
            </Box>
            <Flex direction='column' w='50%' ml='10'>
                <Box>
                    Lovelace: {lovelace}
                </Box>
                <Box>
                    Gimbals: {gimbals}
                </Box>
                <Box>
                    Access Token: {toStr(fromHex(accessToken))}
                </Box>
            </Flex>

        </Flex>
    )
}

export default CommittedBounty