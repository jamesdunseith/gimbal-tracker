import React, { useEffect, useState } from "react";
import { StaticQuery, graphql, Link } from "gatsby";
import { Heading, Text, Box, Flex, Spacer } from "@chakra-ui/react";

const BountyCard = (props) => {
    const frontmatter = props.info

    let bountyColor = 'gl-yellow'
    if (frontmatter.tags.includes("Committed")) { bountyColor = 'red.200' }
    if (frontmatter.tags.includes("Complete")) { bountyColor = 'gl-green' }

    return (
        <Link to={frontmatter.slug}>
            <Flex direction='column' w='100%' p='2' bg={bountyColor} key={frontmatter.slug}>
                <Box mb='3'>
                    <Heading size='md'>
                        {frontmatter.title}
                    </Heading>
                </Box>
                <Flex direction='row' m='1'>
                    <Box>
                        <Heading size='sm' py='1'>
                            {frontmatter.slug}
                        </Heading>
                        <Text>
                            {frontmatter.date}
                        </Text>
                        <Text>
                            {frontmatter.scope}
                        </Text>
                        {frontmatter.tags.map(tag => <Text size='xs'>{tag}</Text>)}

                    </Box>
                    <Spacer />
                    <Box bg='white' mt='5' p='1'>
                        <Text fontSize='lg'>
                            {frontmatter.gimbals} gimbals
                        </Text>
                        <Text>
                            + {frontmatter.ada} ada
                        </Text>
                    </Box>

                </Flex>
            </Flex>
        </Link>
    )
}

export default BountyCard