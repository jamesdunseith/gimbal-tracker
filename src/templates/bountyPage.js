import React, { useEffect, useState } from "react"
import { graphql, Link } from "gatsby";
import {
    Box, Text, Heading, Button, Center, Flex,
    useDisclosure,
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalFooter,
    ModalBody,
    ModalCloseButton,
} from "@chakra-ui/react";
import { useStoreState, useStoreActions } from "easy-peasy";
import useWallet from "../hooks/useWallet"
import { useUtxosFromAddress } from "../hooks/useUtxosFromAddress"
import { createBountyDatum, createTreasuryDatum } from "../utils/factory"
import { fromBech32, toStr, fromHex } from "../utils/converter";
import { serializeBountyDatum, deserializeBounty } from "../cardano/treasury-contract/datums";
import { treasuryIssuerAddress, treasuryContractAddress, bountyContractAddress, commitToBounty, accessPolicyID } from "../cardano/treasury-contract";

// Get markdown styles
import * as style from "./bountyDetails.module.css"

const Template = ({ data, pageContext }) => {
    // Get Bounty Data from Markdown FrontMatter
    const { markdownRemark } = data
    const { next, prev } = pageContext
    const title = markdownRemark.frontmatter.title
    const date = markdownRemark.frontmatter.date
    const slug = markdownRemark.frontmatter.slug
    const completed = markdownRemark.frontmatter.completed
    const tags = markdownRemark.frontmatter.tags
    const scope = markdownRemark.frontmatter.scope
    const ada = markdownRemark.frontmatter.ada
    const lovelace = ada * 1000000
    const gimbals = markdownRemark.frontmatter.gimbals
    const html = markdownRemark.html

    // Get connected wallet from Easy Peasy
    const { wallet } = useWallet(null)
    const connected = useStoreState((state) => state.connection.connected);
    const accessTokens = useStoreState((state) => state.accessTokens.tokenNames);
    // This hook is ready to be used in a selection form on the Commit to Bounty modal
    // Look for this task in the current list of Bounties!
    // For now, just set the currentAccessToken to the first element in the array:
    const [currentAccessToken, setCurrentAccessToken] = useState("")

    // Create a hook to hold walletUtxos. We need this when we pass utxosParam to bountyCommitment.
    const [walletUtxos, setWalletUtxos] = useState([]);

    // Use this hook to query the Treasury Contract address
    const {
        utxos: treasuryUtxos,
        getUtxos,
        loading: utxosLoading,
        error,
    } = useUtxosFromAddress();

    // Create a modal
    // TODO: Add Success notifications and Error reporting via additional modals.
    const { isOpen, onOpen, onClose } = useDisclosure()

    // Use useUtxosFromAddress hook to grab the UTXO from Treasury Contract
    // + setWalletUtxos
    useEffect(async () => {
        if (connected && wallet) {
            await getUtxos({
                variables: {
                    addr: treasuryContractAddress,
                },
            });
            const myUtxos = await wallet.utxos;
            setWalletUtxos(myUtxos);
        }
    }, [wallet])

    useEffect(() => {
        if (accessTokens.length > 0){
            setCurrentAccessToken(toStr(fromHex(accessTokens[0].substring(56))))
        }
    }, [accessTokens])


    // Extract some info from the Treasury UTXO
    const [lovelaceAtTreasury, setLovelaceAtTreasury] = useState("0")
    const [tokensAtTreasury, setTokensAtTreasury] = useState("0")
    const [txHashAtTreasury, setTxHashAtTreasury] = useState(null)
    const [txIxAtTreasury, setTxIxAtTreasury] = useState("0")
    useEffect(async () => {
        setLovelaceAtTreasury(treasuryUtxos?.utxos[0].value)
        setTokensAtTreasury(treasuryUtxos?.utxos[0].tokens[0].quantity)
        setTxHashAtTreasury(treasuryUtxos?.utxos[0].transaction.hash)
        setTxIxAtTreasury(treasuryUtxos?.utxos[0].index)
    }, [treasuryUtxos])

    // main
    const handleBountyCommitment = async () => {
        try {
            console.log("BUILD A BOUNTY COMMITMENT")
            // construct Datum. We need one for each contract: Treasury + Bounty
            // Treasury Datum does not really "do" anything yet:
            const tDatum = createTreasuryDatum("101", treasuryIssuerAddress)
            // Bounty Datum
            const bDatum = createBountyDatum(treasuryIssuerAddress, wallet.address, lovelace, gimbals, 10000000000)
            // Construct a Treasury UTXO that can be serialized in commitToBounty()
            const tUtxo = {
                "tx_hash": txHashAtTreasury,
                "output_index": txIxAtTreasury,
                "amount": [
                    { "unit": "lovelace", "quantity": `${lovelaceAtTreasury}` },
                    { "unit": "cb4a5cb63378a521cb82bdfacc4a8fd543b22ae19c094b75e13f78537447696d62616c", "quantity": `${tokensAtTreasury}` }
                ],
            }

            const bountyCommitment = {
                contributorAddress: fromBech32(wallet.address),
                utxosParam: walletUtxos,
                bountySlug: slug, // need a hook that keeps current selected?
                accessTokenName: currentAccessToken,
                bAda: ada, // from frontmatter
                bGimbals: gimbals, // from frontmater
                tUtxo,
                tLovelaceIn: lovelaceAtTreasury,
                tGimbalsIn: tokensAtTreasury
            }
            console.log("Here is your bountyCommitment:", bountyCommitment)

            // --- Not Essential - Playing with Datum: ---
            // serialize
            const serDatum = serializeBountyDatum(bDatum)
            console.log("Just to show that this works, here is your serialized Datum", serDatum)
            // deserialize
            console.log("And here is your deserialized Datum", deserializeBounty(serDatum))

            // see treasury-contract/index.js:
            commitToBounty(bDatum, tDatum, bountyCommitment)
        } catch (error) {
            console.log("HANDLE BOUNTY ERROR", error)
        }
    }

    return (
        <>
            <Box mx='auto' my='10' p='5' bg="white" w='50%'>
                <Heading size='lg'>
                    {title}
                </Heading>
                <Heading size='sm' py='1'>
                    Posted: {date} {completed ? `| Completed: ${completed}` : ""} | gimbals: {gimbals} | ada: {ada}
                </Heading>

                <Flex direction='row' my='2'>
                    {tags?.map((tag) => <Box bg='orange.400' p='2' mx='2' w="15%" textAlign='center' rounded='md'>{tag}</Box>)}
                    <Box bg='purple.700' color='white' fontWeight='bold' p='2' mx='2' w='15%' textAlign='center' rounded='md'>{scope}</Box>
                </Flex>

                {connected ? (
                    <Box bg="purple.200" p='5'>
                        <Button onClick={onOpen}>Commit to Bounty {slug}</Button>
                    </Box>
                ) : (
                    <Box bg="red.200" p='5'>
                        To commit to a bounty, you must connect a wallet that holds an access token.
                    </Box>
                )}


                <Box bg='orange.100' mt='5' p='5'>
                    <div dangerouslySetInnerHTML={{ __html: html }} className={style.mdStyle} />
                </Box>

                <Center pt='10'>
                    {prev &&
                        <Link to={`/bounties/${prev.frontmatter.slug}`}>
                            <Button mx='3' border='solid' borderColor='gl-blue'>
                                Previous
                            </Button>
                        </Link>
                    }
                    {next &&
                        <Link to={`/bounties/${next.frontmatter.slug}`}>
                            <Button mx='3' border='solid' borderColor='gl-blue'>
                                Next
                            </Button>
                        </Link>
                    }
                    <Link to="/bounties">
                        <Button mx='3' bg='gl-green' border='solid' borderColor='gl-green'>
                            View All
                        </Button>
                    </Link>
                </Center>
                <Box border='1px' mt='5' p='5'>
                    <Heading>How it Works</Heading>
                    <Box bg="purple.200" p='5'>
                        <Heading size='lg'>Bounty Escrow Datum (and, because they have the same structure, Treasury Redeemer) Requires</Heading>
                        <Text py='1'>1. A Bounty "Issuer's" Address and public key hash</Text>
                        <Text py='1'>2. If you hold an Access Token, you can be a "Contributor". We need the public key hash from your connected wallet.</Text>
                        <Text py='1'>3. Ada in Bounty (from frontmatter) = {ada}</Text>
                        <Text py='1'>4. Gimbals in Bounty (from frontmatter) = {gimbals}</Text>
                        <Text py='1'>5. Expiration time (this is not yet implemented)</Text>
                        <Text py='1'>6. Add the slug as metadata</Text>
                    </Box>
                    <Box bg="green.200" p='5'>
                        <Heading>Show how datum works</Heading>
                        <Heading size='sm' py='2'>We can build and serialize it:</Heading>
                        <Heading size='sm' py='2'>Then deserialize it</Heading>
                        <Heading size='sm' py='2'>If all that works, we need to see the redeemer</Heading>
                        <Button onClick={handleBountyCommitment}>Build Datum</Button>
                    </Box>
                    <Box bg="orange.200" p='5'>
                        <Heading>What's in the Treasury?</Heading>
                        <Text>this much lovelace: {lovelaceAtTreasury}</Text>
                        <Text>this much gimbals: {tokensAtTreasury}</Text>
                        <Text>These funds are in the TX: {txHashAtTreasury}#{txIxAtTreasury})</Text>
                        <Text>And i am the issuer: {treasuryIssuerAddress}</Text>
                    </Box>
                </Box>
                <Modal isOpen={isOpen} onClose={onClose}>
                    <ModalOverlay />
                    <ModalContent>
                        <ModalHeader>Commit To Bounty</ModalHeader>
                        <ModalCloseButton />
                        <ModalBody>
                            <Heading size='sm'>Bounty Id:</Heading>
                            <Text py='2'>
                                {slug} (to be included as transaction metadata)
                            </Text>
                            <Heading size='sm'>Current Access Token:</Heading>
                            <Text py='2'>
                                {currentAccessToken}
                            </Text>
                            <Heading size='sm'>Your Access Token:</Heading>
                            {accessTokens?.map(asset => {
                                const assetString = asset.substring(56)
                                const assetName = toStr(fromHex(assetString))
                                return (
                                    <Text py='2'>{assetName}</Text>
                                )
                            })}
                            <Heading size='sm'>Completion Deadline:</Heading>
                            <Text py='2'>
                                date
                            </Text>
                        </ModalBody>

                        <ModalFooter>
                            <Button colorScheme='blue' mr={3} onClick={onClose}>
                                Close
                            </Button>
                            <Button colorScheme='purple' onClick={handleBountyCommitment}>Commit to this Bounty</Button>
                        </ModalFooter>
                    </ModalContent>
                </Modal>
            </Box>
        </>
    )
}

export const query = graphql`
    query($pathSlug: String!) {
        markdownRemark(frontmatter: { slug: { eq: $pathSlug } }) {
            html
            frontmatter {
                title
                date
                completed
                tags
                scope
                ada
                gimbals
                slug
            }
        }
    }
`



export default Template