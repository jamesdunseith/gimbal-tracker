import Cardano from "../cardano/serialization-lib";
import { toHex } from "./converter";

export const createBountyDatum = (issuerAddr, contributorAddr, lovelace, gimbals, expTime) => {
  // issuerAddr = Bech32Address
  // contributorAddr = Bech32Address
  // lovelaceAmount = lovelace
  // tokenAmount = play
  // expirationTime = placeholder value for now

  console.log("creating bounty datum")
  if (issuerAddr && contributorAddr && lovelace && gimbals && expTime) {
    return {
      issuer: getAddressKeyHash(issuerAddr),
      contributor: getAddressKeyHash(contributorAddr),
      lovelaceAmount: lovelace,
      tokenAmount: gimbals,
      expirationTime: expTime
    };
  }
};

export const createTreasuryDatum = (count, issuerAddr) => {
  // issuerAddr = Bech32Address
  // count can be used to keep track of # of bounties; not implementing yet

  console.log("creating treasury datum")
  if (issuerAddr && count) {
    return {
      bountyCount: count,
      treasuryKey: getAddressKeyHash(issuerAddr),
    };
  }
};


export const getAddressKeyHash = (address) => {
  return toHex(
    Cardano.Instance.BaseAddress.from_address(
      Cardano.Instance.Address.from_bech32(address)
    )
      .payment_cred()
      .to_keyhash()
      .to_bytes()
  );
};
