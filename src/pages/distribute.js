import React, { useEffect, useState } from "react"
import { graphql } from "gatsby"
import { useStoreState } from "easy-peasy"
import { Flex, Heading, Text, Box } from "@chakra-ui/react"
import { useUtxosFromAddress } from "../hooks/useUtxosFromAddress"
import useWallet from "../hooks/useWallet"
import { bountyContractAddress } from "../cardano/treasury-contract"
import CommittedBounty from "../components/CommittedBounty"
import Complete from "../components/Bounty/Complete"

const Distribute = () => {

  // Get connected wallet from Easy Peasy
  const { wallet } = useWallet(null)
  const connected = useStoreState((state) => state.connection.connected);
  const [walletUtxos, setWalletUtxos] = useState([]);

  // Use this hook to query the Bounty Contract address
  const {
    utxos: bountyUtxos,
    getUtxos,
    loading: utxosLoading,
    error,
  } = useUtxosFromAddress();

  useEffect(async () => {
    if (connected && wallet) {
      await getUtxos({
        variables: {
          addr: bountyContractAddress,
        },
      });
      const myUtxos = await wallet.utxos;
      setWalletUtxos(myUtxos);
    }
  }, [wallet])

  // Map each Bounty UTXO to a consumable object that can be passed as prop to <CommittedBounty />
  // What is like this?

  return (
    <>
      <title>DISTRIBUTE</title>
      <Flex
        w="100%"
        mx="auto"
        direction="column"
        wrap="wrap"
        bg="gl-yellow"
        p="10"
      >
        <Box w="50%" mx="auto" my="5">
          <Heading py='5'>Issuer can unlock Bounties</Heading>
          <Text py='2'>1. Show list of commited bounties - ie, UTXOs locked at Bounty Contract here. Include details.</Text>
          <Text py='2'>2. Build out /src/cardano/bounty-contract</Text>
          <Text py='2'>3. What are our options when we only have file key pair for "Issuer"?</Text>
          <Text py='2'>4. How can we build ways for "Issuer" to identify themselves -- should probably add a token for this as well.</Text>
          <Text py='2'>5. For now, just make sure this works with Issuer from front end</Text>
          {/* <Text>{JSON.stringify(bountyUtxos)}</Text> */}
          {bountyUtxos?.utxos.map(i => <CommittedBounty utxo={i} />)}
        </Box>
      </Flex>
      <Complete />
    </>
  )
}

// we can grab Bounty Amounts from frontmatter: ada, gimbals

export const query = graphql`
  query GetTitles {
    allMarkdownRemark(sort: { order: ASC, fields: [frontmatter___slug] }) {
      edges {
        node {
          frontmatter {
            title
            slug
            ada
            gimbals
          }
        }
      }
    }
  }
`

export default Distribute
