import React, { useEffect, useState } from "react"
import { useStoreState } from "easy-peasy"
import { Flex, Heading, Text, Box, Button } from "@chakra-ui/react"
import { useUtxosFromAddress } from "../hooks/useUtxosFromAddress"
import useWallet from "../hooks/useWallet"
import { treasuryContractAddress, treasuryIssuerAddress } from "../cardano/treasury-contract"
import CommittedBounty from "../components/CommittedBounty"
import { lockingTx } from "../cardano/locking-tx"

const Treasury = () => {

    // Get connected wallet from Easy Peasy
    const { wallet } = useWallet(null)
    const connected = useStoreState((state) => state.connection.connected);
    const [walletUtxos, setWalletUtxos] = useState([]);

    // Hard code for now. Build a form later:
    const lovelaceToLock = 50000000;
    const gimbalsToLock = 4000;

    // Use this hook to query the Bounty Contract address
    const {
        utxos: treasuryUtxos,
        getUtxos,
        loading: utxosLoading,
        error,
    } = useUtxosFromAddress();

    useEffect(async () => {
        if (connected && wallet) {
            await getUtxos({
                variables: {
                    addr: treasuryContractAddress,
                },
            });
            const myUtxos = await wallet.utxos;
            setWalletUtxos(myUtxos);
        }
    }, [wallet])

    const handleLockFundsInTreasury = async () => {
        try {
            const treasuryLockParams = {
                address: connected,
                utxosParam: walletUtxos,
                lovelace: lovelaceToLock,
                gimbals: gimbalsToLock
            }
            lockingTx(treasuryLockParams)
        } catch (error) {
            console.log("Error locking Treasury Funds", error)
        }
    }

    return (
        <>
            <title>TREASURY</title>
            <Flex
                w="100%"
                mx="auto"
                direction="column"
                wrap="wrap"
                bg="gl-yellow"
                p="10"
            >
                <Box w="50%" mx="auto" my="5">
                    <Heading py='5'>Issuer can Lock Funds in Treasury</Heading>
                    {connected == treasuryIssuerAddress ? (
                        <Box bg='blue.200' m='5' p='5'>
                            <Heading>Fund a Treasury</Heading>
                            <Text py='5'>
                                You are the Bounty Issuer specified for this Treasury, so you can lock funds in the Treasury.
                            </Text>
                            <Heading size='lg'>What is in the treasury now?</Heading>
                            <Text>{JSON.stringify(treasuryUtxos)}</Text>
                            <Text p='1'>
                                By pressing "Fund Treasury", you will lock:
                            </Text>
                            <Text p='1'>
                                {lovelaceToLock} lovelace
                            </Text>
                            <Text p='1'>
                                {gimbalsToLock} gimbals
                            </Text>
                            <Text p='1'>
                                Number UTXOs in connected wallet: {walletUtxos.length}
                            </Text>
                            <Button m='5' onClick={handleLockFundsInTreasury}>Fund Treasury</Button>
                        </Box>

                    ) : (
                        <Box bg='blackAlpha.900' m='5' p='5' color='white'>You are not the Bounty Issuer for this Treasury, so there is nothing to see here.</Box>
                    )}
                </Box>
            </Flex>
        </>
    )
}


export default Treasury
